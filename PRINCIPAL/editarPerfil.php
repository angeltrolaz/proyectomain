<?php
session_start();
$varsesion=$_SESSION['nombre_usuario'];
if($varsesion==null || $varsesion= ''){
echo 'ILEGAL... NO as ingresado datos para iniciar sesion!!';
die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
		<title>Menu De Navegacion Con Listas</title>
		<link rel="stylesheet" type="text/css" href="ESTILOS/estilo2.css">
        <script src="validarregistro.js"></script>
</head>
<body>
<nav>
  <img src="logouleam.png" alt="Logo de tu sitio web">
  <ul class="gestor">
  <li><a href="#" onclick="location.reload();">Facultades</a></li>
</ul>
<form id="search-form" action="#" method="get">
  <input type="search" placeholder="Buscar...">
  <button type="submit">Buscar</button>
</form>
  <ul> 
  <li>
    <a href="#" class="activos-fijos">Activos Fijos</a>
    <ul>
      <li><a href="MENU/edificios.php">Edificios y estructuras</a></li>
      <li><a href="MENU/equipamiento.php">Equipamiento y mobiliario</a></li>
      <li><a href="MENU/vehiculos.php">Vehículos</a></li>
      <li><a href="MENU/tecnologia.php">Tecnología</a></li>
    </ul>
  </li>
  <li>
    <a href="#" class="bienvenido"><?php echo $_SESSION['nombre_completo'] ?></a>
    <ul>
      <li><a href="#">Configuración</a></li>
      <li><a href="../DB/cerrarsesion.php">Salir</a></li>
    </ul>
  </li>
</nav>
<div class="EDITAR-USUARIO">
<form onsubmit="return Validarres()">
  <h1><img src="USUARIO.png" alt="nombre de la imagen">Configuración Cuenta</h1>
  <label for="opcion">Seleccione una opción:</label>
  <select id="opcion" name="opcion">
    <option value="usuario">Editar usuario</option>
    <option value="contra">Editar contraseña</option>
    <option value="nombrescompletos">Editar nombres y apellidos</option>
    <option value="email">Editar correo electrónico</option>
    <option value="telefono">Editar teléfono</option>
    <option value="direccion">Editar dirección</option>
  </select>
  <div id="campo-usuario" style="display:none;">
    <input type="text" name="usuario" placeholder="Nuevo usuario">
  </div>
  <div id="campo-contra" style="display:none;">
    <input type="password" name="contra" placeholder="Nueva contraseña">
  </div>
  <div id="campo-nombrescompletos" style="display:none;">
    <input type="text" name="nombrescompletos" placeholder="Nuevos nombres y apellidos">
  </div>
  <div id="campo-email" style="display:none;">
    <input type="email" name="email" placeholder="Nuevo correo electrónico">
  </div>
  <div id="campo-telefono" style="display:none;">
    <input type="tel" name="telefono" placeholder="Nuevo teléfono">
  </div>
  <div id="campo-direccion" style="display:none;">
    <input type="text" name="direccion" placeholder="Nueva dirección">
  </div>
  <input type="submit" name="register">
</form>

<script>
  var opcion = document.getElementById("opcion");
  opcion.addEventListener("change", function() {
    var campo = document.getElementById("campo-" + opcion.value);
    if (campo) {
      var campos = document.querySelectorAll("[id^='campo-']");
      campos.forEach(function(c) {
        c.style.display = "none";
      });
      campo.style.display = "block";
    }
  });
</script>
</div>
</html>
