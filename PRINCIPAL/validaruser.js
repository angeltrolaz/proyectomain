function validarFormulario() {
    // Obtener los valores de los campos del formulario
    const usuario = document.getElementsByName('usuario')[0].value.trim();
    const contra = document.getElementsByName('contra')[0].value.trim();
  
    // Expresiones regulares para validar los campos del formulario
    const regexUsuario = /^[a-zA-Z0-9._-]+$/; // Solo letras, números, puntos, guiones bajos y guiones medios
    const regexContra = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/; // Mínimo 8 caracteres, al menos una letra mayúscula, una letra minúscula y un número
  
    // Validar el campo "usuario"
    if (!regexUsuario.test(usuario)) {
      alert('Por favor, ingrese un nombre de usuario válido (solo letras, números, puntos, guiones bajos y guiones medios)');
      return false;
    }
  
    // Validar el campo "contra"
    if (!regexContra.test(contra)) {
      alert('Por favor, ingrese una contraseña válida (mínimo 8 caracteres, al menos una letra mayúscula, una letra minúscula, un número y un carácter especial)');
      return false;
    }
  
    // Si todos los campos son válidos, devolver "true" para enviar el formulario
    return true;
  }
  