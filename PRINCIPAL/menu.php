<nav class="menu-principal">
  <img src="logouleam.png" alt="Logo de tu sitio web">
  <ul class="gestor">
    <li><a href="ingreso.php" onclick="location.reload();">Facultades</a></li>
  </ul>
  <form id="search-form" action="#" method="get">
    <input type="search" placeholder="Buscar...">
    <button type="submit">Buscar</button>
  </form>
  <ul> 
    <li>
      <a href="#" class="activos-fijos">Activos Fijos</a>
      <ul>
        <li><a href="#">Edificios y estructuras</a></li>
        <li><a href="#">Equipamiento y mobiliario</a></li>
        <li><a href="#">Vehículos</a></li>
        <li><a href="#">Tecnología</a></li>
      </ul>
    </li>
    <li>
      <a href="#" class="bienvenido"><?php echo $_SESSION['nombre_completo'] ?></a>
      <ul>
        <li><a href="#">Configuración</a></li>
        <li><a href="../DB/cerrarsesion.php">Salir</a></li>
      </ul>
    </li>
  </ul>
</nav>