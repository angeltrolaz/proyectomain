// Obtiene el elemento del formulario con el id 'search-form'
const form = document.getElementById('search-form');

// Agrega un evento para escuchar el envío del formulario
form.addEventListener('submit', function(event) {

  // Previene que el formulario se envíe
  event.preventDefault();

  // Obtiene el término de búsqueda del elemento input del formulario y lo convierte en minúsculas
  const searchTerm = form.querySelector('input[type="search"]').value.toLowerCase();

  // Obtiene todos los elementos con la clase 'cuadro' dentro del elemento con la clase 'contenedor-cuadros'
  const cuadros = document.querySelectorAll('.contenedor-cuadros .cuadro');

  // Itera a través de cada 'cuadro'
  cuadros.forEach(cuadro => {

    // Obtiene el título del 'cuadro' y lo convierte en minúsculas
    const cuadroTitle = cuadro.querySelector('h2').textContent.toLowerCase();

    // Obtiene todos los elementos de lista dentro del 'cuadro'
    const cuadroItems = cuadro.querySelectorAll('li');

    // Crea una variable booleana para rastrear si se encontró una coincidencia
    let matchFound = false;

    // Itera a través de cada elemento de lista en el 'cuadro'
    cuadroItems.forEach(item => {

      // Obtiene el texto del elemento de lista y lo convierte en minúsculas
      const itemText = item.textContent.toLowerCase();

      // Verifica si el término de búsqueda está incluido en el texto del elemento de lista
      if (itemText.includes(searchTerm)) {
        matchFound = true;
      }
    });

    // Verifica si el término de búsqueda está incluido en el título del 'cuadro' o si se encontró una coincidencia en los elementos de lista
    if (cuadroTitle.includes(searchTerm) || matchFound) {

      // Si se encontró una coincidencia, muestra el 'cuadro'
      cuadro.style.display = 'block';
    } else {

      // Si no se encontró una coincidencia, oculta el 'cuadro'
      cuadro.style.display = 'none';
    }
  });
});

