function guardarRutaImagen(elemento) {
  var ruta = elemento.parentNode.querySelector('img').getAttribute('src');
  localStorage.setItem('rutaImagen', ruta);
}