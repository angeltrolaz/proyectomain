<?php
session_start();
$varsesion=$_SESSION['nombre_usuario'];
if($varsesion==null || $varsesion= ''){
echo 'ILEGAL... NO as ingresado datos para iniciar sesion!!';
die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
		<title>Menu De Navegacion Con Listas</title>
		<link rel="stylesheet" type="text/css" href="../ESTILOS/estilo2.css">
</head>
<body>
<nav>
  <img src="../logouleam.png" alt="Logo de tu sitio web">
  <ul class="gestor">
  <li><a href="../ingreso.php" onclick="location.reload();">Facultades</a></li>
</ul>
<form id="edificio-form" action="#" method="get">
  <input type="search" placeholder="Buscar Edificio...">
  <button type="submit">Buscar</button>
</form>
  <ul> 
  <li>
    <a href="#" class="activos-fijos">Activos Fijos</a>
    <ul>
      <li><a href="edificios.php">Edificios y estructuras</a></li>
      <li><a href="equipamiento.php">Equipamiento y mobiliario</a></li>
      <li><a href="vehiculos.php">Vehículos</a></li>
      <li><a href="tecnologia.php">Tecnología</a></li>
    </ul>
  </li>
  <li>
    <a href="#" class="bienvenido"><?php echo $_SESSION['nombre_completo'] ?></a>
    <ul>
      <li><a href="#">Configuración</a></li>
      <li><a href="../../DB/cerrarsesion.php">Salir</a></li>
    </ul>
  </li>
</nav>
<div class="EDIFICIO">
  <img src="../CIENCIASEDUCACION.png" alt="Imagen de Ciencias de la Educación">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias de la Educación</a></h1>

</div>

<div class="EDIFICIO">
  <img src="../PSICOLOGIA.png" alt="Imagen de Psicología">
  <h1> <a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Psicología</a></h1>
</div>

<div class="EDIFICIO">
  <img src="../TRABAJOSOCIAL.png" alt=" Imagen Trabajo Social">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Trabajo Social</a></h1>
</div>

<div class="EDIFICIO">
  <img src="../cienciascomunicacion.png" alt="Imagen de Ciencias Comunicacion">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias Comunicacion</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../DERECHO.jpg" alt="Imagen de Derecho">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Derecho</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../CONTABILIDADYAUDITORIA.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Contabilidad y Auditoría</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../CIENCIASADMINISTRATIVAS.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias Administrativas</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../CIENCIASECONOMICAS.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias Económicas</a></h1>
</div>

<div class="EDIFICIO">
  <img src="../CIENCIASMAR.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias del Mar</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../CIENCIASINFORMATICAS.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias Informáticas</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../INGENIERIA.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ingeniería</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../agropecuaria.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias Agropecuarias</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../INDUSTRIAL.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ingeniería Industrial</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../ARQUITECTURA.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Arquitectura</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../MEDICINA.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Ciencias Médicas</a></h1>
</div>
<div class="EDIFICIO">
  <img src="../ODONTOLOGIA.png" alt="Imagen de naturaleza">
  <h1><a href="FormEdificio.php"onclick="guardarRutaImagen(this)">Odontología</a></h1>
</div>
<script src="filtroedificios.js"></script>

<script>function guardarRutaImagen(elemento) {
  var ruta = elemento.parentNode.querySelector('img').getAttribute('src');
  localStorage.setItem('rutaImagen', ruta);
}
</script>
</body>
</html>