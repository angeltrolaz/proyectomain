document.addEventListener("DOMContentLoaded", function(event) {
  var form = document.querySelector("#edificio-form");
  form.addEventListener("submit", function(event) {
    event.preventDefault();
    var searchQuery = form.querySelector("input[type=search]").value.toLowerCase();
    var edificios = document.querySelectorAll(".EDIFICIO");
    edificios.forEach(function(edificio) {
      var nombre = edificio.querySelector("h1").textContent.toLowerCase();
      if (nombre.indexOf(searchQuery) !== -1) {
        edificio.style.display = "block";
      } else {
        edificio.style.display = "none";
      }
    });
  });
});