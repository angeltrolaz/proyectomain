<?php
session_start();
$varsesion=$_SESSION['nombre_usuario'];
if($varsesion==null || $varsesion= ''){
echo 'ILEGAL... NO as ingresado datos para iniciar sesion!!';
die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
		<title>Menu De Navegacion Con Listas</title>
		<link rel="stylesheet" type="text/css" href="../ESTILOS/estilo2.css">
        <script> src="copiarurl.js"</script>
</head>
<body>
<nav>
  <img src="../logouleam.png" alt="Logo de tu sitio web">
  <ul class="gestor">
  <li><a href="../ingreso.php" onclick="location.reload();">Facultades</a></li>
</ul>
<form id="edificio-form" action="#" method="get">
  <input type="search" placeholder="Buscar Edificio...">
  <button type="submit">Buscar</button>
</form>
  <ul> 
  <li>
    <a href="#" class="activos-fijos">Activos Fijos</a>
    <ul>
      <li><a href="edificios.php">Edificios y estructuras</a></li>
      <li><a href="equipamiento.php">Equipamiento y mobiliario</a></li>
      <li><a href="vehiculos.php">Vehículos</a></li>
      <li><a href="tecnologia.php">Tecnología</a></li>
    </ul>
  </li>
  <li>
    <a href="#" class="bienvenido"><?php echo $_SESSION['nombre_completo'] ?></a>
    <ul>
      <li><a href="#">Configuración</a></li>
      <li><a href="../../DB/cerrarsesion.php">Salir</a></li>
    </ul>
  </li>
</nav>
<div class="formularioEdificio">
<img id="edificio-img" src="../CIENCIASEDUCACION.png">
      <form>
        <label for="identificacion">Identificación:</label><br>
        <input type="text" id="identificacion" name="identificacion"placeholder="0000"><br>
        <label for="descripcion">Descripción:</label><br>
        <input type="text" id="descripcion" name="descripcion"placeholder="Tamaño, número de pisos"><br>
        <label for="valor">Valor:</label><br>
        <input type="number" id="valor" name="valor"placeholder="$ 00000.00000"><br>
        <label for="depreciacion">Depreciación:</label><br>
        <input type="number" id="depreciacion" name="depreciacion"placeholder="Tasa Depreciacion"><br>
        <label for="mantenimiento">Mantenimiento:</label><br>
        <input type="text" id="mantenimiento" name="mantenimiento"placeholder="Actividades de mantenimiento realizadas en el edificio"><br><br>
        <input type="submit" value="Enviar">
      </form>
    </div>
</body>
</html>

